//  [Section] Comments	


//  There are two types of comments:
// -Single line comment (ctrl +/) denoted by //

/*
	- multiline comment
	- denoted by / ** /
	- ctrl + shift + /
*/
// [Section] Statements and Syntax
	// Statements
		//  In programming language , statements are instruction that we tell the computer to perform.
		// JS statements usually ends with semicolon (;)
			// (;) is also called as delimeter
		// Semicolons are not required in JS.
			// it use to help us train or locate where a statement ends.
	// Syntax
		 // in programming, its is the set of rules that describe how statements must be constructed.

console.log("Hello World");
 
 // [Section] 
	
	// Syntax: let/const viribleName;
	// let is a keyword that is usually used in declaring a variable
let myVariable;
console.log(myVariable);




// Initialize a value
// Storing the intial value of a variable
	// Assignment Operator (=)
myVariable= "Hello";

// Reassingment a variable value
	// Changing the initial value to a new value
myVariable= "Hello World";

console.log(myVariable);

// const
	// "const" keyword is used to declare and initialized a constant variable.
	// A "constant" variable does not change.
// const PI;
// PI=3.1416;
// console.log(PI) //error : Initial value is missing for const.

// Declaring with initialization
	// a variable is given it's initial/starting value upon declaration.
// Syntax: let/const variable variableName=value;
	let productName="Desktop Computer";
	console.log(productName);
	let productPrice=18999;
	productPrice=20000;
	console.log(productPrice);



	const PI=3.1416;
	console.log(PI);

	/*
		Guide in writing variables.
		1. Use the "let" keyword if the variable will contain different values/can change over time.
			Naming Convention: camelCasing

		2. Use the "const" keyword if the variable does not changed.
			Naming Convention: "const" keyword should be followed by all CAPITAL VARIBLE NAME

		3. Variable names should be indicative (or descriptive) of
			value being stored to avoid confusion
		 4. Variable name are case sensitive



	*/

	// Multiple variable declaration

let productCode="CD017" ,productBrand="Dell";
console.log(productCode,productBrand);

// Using a reserve keyword
// const let="hello";
// console.log(let)

// [Section] Data Type

// In JavaScript, there are six types of data

	// String
		// are series of characters that create a word, a phrase, a sentence or anything related to creating text.
		// String in JavaScript can be written using a single quote('') or double ("")

		let country = "Philippines";
		let provice = 'Metro Manila';

		// Concatenating Strings
		// Multiple string values that can be combined to create a single single String using the "+"

		let greeting = "I live in " + provice + "," + country;
		console.log(greeting);

		// Escape Characters
		// (\) in string combination with other characters can produce different result

		let mailAddress="Quezong City \nPhilippines";
		console.log(mailAddress);

		let message = "John's employess wente home early";
		console.log(message);

		message = 'Jane\'s employess wente home early';
		console.log(message)

		//  Numbers

			// Integers/Whole numbers
			let headCount =26;
			console.log(headCount);

			// Decimal number/Fractions
			let grade = 98.7;
			console.log(grade);

			// Exponential Notation
			let planetDistance =2e10;
			console.log(planetDistance);

			console.log("John's grade"+grade);

		// Boolean
			// Boolean values are normally used to store values relating to the state of certain things.
				// true or false (two logical values)

			let isMarried = false;
			let isGoodConduct= true;

			console.log("isMarried: "+isMarried);
			console.log("isGoodConduct: "+isGoodConduct);

		// Arrays

			// are special kind of data type that can used to store multiple related values.

			// Syntex: let/const arrayName=[elementA, elementB];


			let grades = [98.6,92.1,90.2,94.6];
			console.log(grades);

		// Objects
			// Objects are anothe special kind of data type that is used to mimic real world objects/items.
			// used to create complex data that contains information relavant to each other.

		/*
			Syntax :
			let/const objectName = {
						propertyA: valueA,
						propertyB: valueB,

			} 

		*/

			let person = {
				fullName: "Juan Dela Cruz",
				age : 35,
				isMarried: false,
				contact: ["+6399999999", "093452223456"],
				address : {
					houseNumber: "345",
					city: "Manila"
				}
			};
			console.log(person);

			// Ther're are also useful for creating abstract objects

			let myGrades ={
				firstGrading: 98.7,
				secondGrading: 92.1,
				thirdGrading: 90.2,
				fourthGrading: 94.6

			};

			console.log(myGrades);

			// typeof operator is used to determined the type of data or the value of a variable.

			console.log(typeof myGrades);

			// arrays is a special type of object with methods.
			console.log(typeof grades);

			// Null
			// indicates the absence of a value,
			let spouse =null;
